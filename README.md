# Static Page as Interface of Disbursement API

## Requirements

- python3

## How to run Local server

- `python3 -m http.server`

## Live Version

[Link Netlify](https://disbursement-services.netlify.app/)
